# Информационные ресурсы пиратской партии

* https://pirate-party.ru/
* https://wiki.pirate-party.ru/
* https://forum.pirate-party.ru/
* https://piratemedia.net/
* https://podcast.pirate-party.ru/

# Ресурсы в социальных сетях

* https://twitter.com/ru_pirateparty
* https://t.me/chatppru
* https://vk.com/club10354961
* https://www.facebook.com/rupirate
* https://rutracker.org/forum/viewforum.php?f=2448
* https://www.youtube.com/channel/UCpACYGzevN3d3d1IpaDTEDQ

